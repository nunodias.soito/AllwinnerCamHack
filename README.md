# 4K Action Camera - Allwinner V3 FW modding

### Firmware Backup
Instructions for firmware backup:

1. Insert the memory card into the camera
2. Connect the camera to the PC and select 'Charging mode'
3. Install ADB drivers if they are not installed
4. Run 'backup.cmd' script
5. A 'full_img.fex' firmware image and partition files (.img) will be generated in the 'backup' folder on the memory card
Note: For Windows 8 and 10, driver signature verification must be disabled to be able to install ADB drivers.

### Firmware Upgrade
Instructions for firmware upgrade:

1. Copy 'full_img.fex' image file to the root of the memory card
2. Turn on the camera and navigate to the configuration view that provides the current firmware information
3. Press 'UP' and confirm to start the firmware installation

### Customize boot and power-off logos
Instructions for modifying boot and shutdown logos:

1. Obtain firmware image files using the 'backup.cmd' script
2. Get JPEG images with 320x240 resolution: blogo.jpg (boot logo) + slogo.jpg (shutdown logo)
3. Copy JPEG images and firmware image files to 'UNFEX' folder
4. Run 'jpg2img.bat' script to convert the logos to '4-blogo.img' and '5-blogo.img' files
5. Use 'refex.bat' script to get a new 'full_img.fex' from the .img files
6. Copy 'full_img.fex' file to the root of the SD card and install the modified firmware
Note: Each JPEG file size should not exceed 131072 bytes and picture resolution must be 320x240 pixels. Both files should not be saved with progressive baseline compression format.

VIDEO: ![[Q3H-2] Allwinner V3 Action Camera - Customize boot and power off logos](https://www.youtube.com/watch?v=e-reYQOCgkM)


## Download Links
You can get all script files and tool working properly on:

* **ADB drivers:** [Download](https://www.mediafire.com/file/1z4g3h58a3qzs4q/1-ADB_driver.rar/file)
* **ADB recovery drivers:** [Download](https://www.mediafire.com/file/vd1l2r354595214/1-ADB_recovery_driver.rar/file)
* **Driver signature scripts:** [Download](https://www.mediafire.com/file/bg2tb47idx7sdob/driversignature_scripts.rar/file)
* **Backup script:** [Download](https://www.mediafire.com/file/6xpdzi4c4hbj6bb/2-ROM_Backup_script_26Dec2016.rar/file)
* **Scripts (unfex, refex, ...):** [Download](https://www.mediafire.com/file/1oo11xp6x9qtj6a/3-unfex-scripts_19Fev2017.rar/file)
* **Sample logos, ...:** [Download](https://www.mediafire.com/folder/upm2r7ap6w5q6/Edit)



```
Check GoPrawn.com action cam discussion forum for more details
```
