::SQUASHFS_MAKE.BAT script by BNDias.
::Check GoPrawn.com for details.

@echo off

:: Set source folder and destination file
set SOURCE=SQUASHFS-ROOT
set DESTINATION=UNFEX\2-system.img
set DESTSIZE=6488064
set TOOLFOLDER=Tools\squashfs_tools
echo SQUASHFS_MAKE.BAT script by BNDias for GoPrawn.com
echo.

if not exist "%~dp0%TOOLFOLDER%\mksquashfs.exe" goto MKSQUASHFSNOTFOUND
if not exist "%~dp0Tools\sfk.exe" goto SFKNOTFOUND
if not exist "%~dp0%SOURCE%" goto SOURCENOTFOUND

setlocal EnableDelayedExpansion
if exist "%~dp0%DESTINATION%" (
	if exist "%~dp0%DESTINATION%.bak"  del /F "%~dp0%DESTINATION%.bak"
	for %%A in ("%~dp0%DESTINATION%") do  set FILENAME=%%~nxA
	ren "%~dp0%DESTINATION%" "!FILENAME!.bak">nul
	echo Existing %DESTINATION% file renamed to %DESTINATION%.bak
	echo.
)
endlocal

echo.
"%~dp0%TOOLFOLDER%\mksquashfs" "%~dp0%SOURCE%" "%~dp0%DESTINATION%" -comp xz -Xbcj x86 -noappend -no-recovery -root-owned
echo.
echo.
echo.
for %%A in ("%DESTINATION%") do set size=%%~zA
if %size% GTR %DESTSIZE% goto BADFILESIZE
if %size% LSS %DESTSIZE% (
	if not exist "%~dp0Tools\DummyFiles\sysdummy.bin" GOTO DUMMYNOTFOUND
	copy /b "%~dp0%DESTINATION%"+"%~dp0Tools\DummyFiles\sysdummy.bin" "%~dp0system.tmp" >nul 2>&1
	"%~dp0Tools\sfk" "partcopy" "%~dp0system.tmp" "0" "%DESTSIZE%" "%~dp0%DESTINATION%" "-yes" "-quiet"
	del "%~dp0system.tmp" >nul 2>&1
)
echo Done. Check "%DESTINATION%" file.

:EXITBAT
echo Press any key to exit...
pause>nul
exit /b


:MKSQUASHFSNOTFOUND
echo.
echo SQUASHFS_TOOLS/MKSQUASHFS.EXE not found
if not exist "%~dp0%TOOLFOLDER%"  mkdir "%~dp0%TOOLFOLDER%"
goto EXITBAT

:SFKNOTFOUND
echo SFK.EXE not found - Please download it from: https://sourceforge.net/projects/swissfileknife/
goto EXITBAT

:SOURCENOTFOUND
echo %SOURCE% folder not found
goto EXITBAT

:BADFILESIZE
echo Error! The size of "%DESTINATION%" file exceeds %DESTSIZE% bytes.
del /F "%~dp0%DESTINATION%">nul
goto EXITBAT

:DUMMYNOTFOUND
echo SYSDUMMY.BIN file not found
del /F "%~dp0%DESTINATION%">nul
goto EXITBAT