::UNPACK_BOOTIMG.BAT script by BNDias.
::Check GoPrawn.com for details.

@echo off

::Set source file and destination folder
set SOURCE=UNFEX\1-boot.img
set DESTINATION=BOOT-ROOT
::Set BOOTIMG tool folder
set TOOLFOLDER=Tools\unpack_repack_bootimg_kernel

::Change working directory
set ROOT=%~dp0
cd "%ROOT%"


echo UNPACK_BOOTIMG.BAT script by BNDias for GoPrawn.com
echo.


if not exist "%TOOLFOLDER%\bootimg.exe"  goto BOOTIMGNOTFOUND
if not exist "%SOURCE%"  goto SOURCENOTFOUND


if exist "%DESTINATION%" (
    if exist "%DESTINATION%_bak"  rd /S /Q "%DESTINATION%_bak"
    move "%DESTINATION%" "%DESTINATION%_bak" >nul
    echo Existing %DESTINATION% folder renamed to %DESTINATION%_bak
    echo.
)
mkdir "%DESTINATION%"


setlocal EnableDelayedExpansion

::Set exclude list (not extracted files/folders)
if exist "%TOOLFOLDER%\ExcludeList.txt"  del /F /Q "%TOOLFOLDER%\ExcludeList.txt" >nul 2>&1
for %%f in ("%TOOLFOLDER%\*") do  echo "%%f" >> "%TOOLFOLDER%\ExcludeList.txt"
for /D %%d in ("%TOOLFOLDER%\*") do  echo "%%d" >> "%TOOLFOLDER%\ExcludeList.txt"
echo "%TOOLFOLDER%\ExcludeList.txt" >> "%TOOLFOLDER%\ExcludeList.txt"

::Copy source file to tool folder
copy /B /Y "%SOURCE%" "%TOOLFOLDER%\boot.img" >nul 2>&1


::Extract BOOT.IMG
cd "%TOOLFOLDER%"
echo Extracting %SOURCE%...
echo ==================================================
bootimg --unpack-bootimg boot.img
echo ==================================================
cd "%ROOT%"


del /F /Q "%TOOLFOLDER%\boot.img"
del /F /Q "%TOOLFOLDER%\boot-old.img"

::Copy extracted content to destination folder
for %%f in ("%TOOLFOLDER%\*") do (
    find "%%f" "%TOOLFOLDER%\ExcludeList.txt" >nul
    if !ERRORLEVEL! == 1  move /y "%%f" "%DESTINATION%" >nul
)
for /D %%d in ("%TOOLFOLDER%\*") do (
    find "%%d" "%TOOLFOLDER%\ExcludeList.txt" >nul
    if !ERRORLEVEL! == 1  move /y "%%d" "%DESTINATION%\" >nul
)
::Remove exclude list
del /F /Q "%TOOLFOLDER%\ExcludeList.txt" >nul 2>&1

echo.
echo Done. Check "%DESTINATION%" folder.
endlocal


:EXITBAT
echo Press any key to exit...
pause>nul
exit /b


:BOOTIMGNOTFOUND
echo.
echo "%TOOLFOLDER%\bootimg.exe" not found
goto EXITBAT

:SOURCENOTFOUND
echo "%SOURCE%" file not found
goto EXITBAT