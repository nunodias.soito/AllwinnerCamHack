::FWINFO.BAT script for gathering Allwinner V3 firmware details by nutsey
::Check GoPrawn.com for details.

@echo off
setlocal
echo Allwinner V3 firmware information script by nutsey v0.1.
echo.

::Set source folders/files:
set SQUASHFSROOT=%~dp0SQUASHFS-ROOT
::Set destination folder/files:
set INFOFILE=%~dp0FWINFO\fwinfo.txt
set BLOGO=%~dp0FWINFO\blogo.jpg
set SLOGO=%~dp0FWINFO\slogo.jpg

if not exist "%~dp0UNFEX\4-blogo.img"  goto LOGONOTFOUND
if not exist "%~dp0UNFEX\5-slogo.img"  goto LOGONOTFOUND
if not exist "%SQUASHFSROOT%\build.prop"  goto BUILDPROPNOTFOUND
if not exist "%SQUASHFSROOT%\res\cfg\320x240.cfg"  goto CFGNOTFOUND
if not exist "%~dp0script.fex"  goto FEXNOTFOUND

if not exist "%~dp0\FWINFO"  md "%~dp0FWINFO" >nul 2>&1
echo Product:  > "%~dp0fwinfo.tmp" && "%~dp0Tools\sfk" extract "%SQUASHFSROOT%\build.prop" -text "/ro.build.product=*/[part2]/" -astext >> "%~dp0fwinfo.tmp"
echo Manufacturer:  >> "%~dp0fwinfo.tmp" && "%~dp0Tools\sfk" extract "%SQUASHFSROOT%\build.prop" -text "/ro.build.user=*/[part2]/" -astext >> "%~dp0fwinfo.tmp"
echo FW date:  >> "%~dp0fwinfo.tmp" && "%~dp0Tools\sfk" extract "%SQUASHFSROOT%\build.prop" -text "/ro.build.version.incremental=*.*.*.*/[part6]/" -astext >> "%~dp0fwinfo.tmp"
echo Version:  >> "%~dp0fwinfo.tmp" && "%~dp0Tools\sfk" extract "%SQUASHFSROOT%\res\cfg\320x240.cfg" -text "/software_version=*/[part2]/" -astext >> "%~dp0fwinfo.tmp"
echo LCD model:  >> "%~dp0fwinfo.tmp" && "%~dp0Tools\sfk" extract "%~dp0script.fex" -text "/lcd_driver_name = \q*\q/[part2]/" -astext >> "%~dp0fwinfo.tmp"
echo Sensor model:  >> "%~dp0fwinfo.tmp" && "%~dp0Tools\sfk" extract "%~dp0script.fex" -text "/vip_dev0_mname = \q*\q/[part2]/" -astext >> "%~dp0fwinfo.tmp"
"%~dp0Tools\sfk" partcopy "%~dp0fwinfo.tmp" -fromto 0 -9 "%INFOFILE%" -yes -quiet
del /F /Q fwinfo.tmp >nul 2>&1

type "%INFOFILE%"
copy /B "%~dp0UNFEX\4-blogo.img" "%BLOGO%" >nul 2>&1
copy /B "%~dp0UNFEX\5-slogo.img" "%SLOGO%" >nul 2>&1
echo.
echo Done. Check FWINFO. Press any key to exit...
pause>nul
exit /b

:LOGONOTFOUND
echo 4-BLOGO.IMG and 5-SLOGO.IMG not found in UNFEX folder! Please run UNFEX script first.
echo Press any key to exit...
pause>nul
exit /b

:FEXNOTFOUND
echo SCRIPT.FEX not found! Please exract SCRIPT.BIN file and convert it to SCRIPT.FEX.
echo Press any key to exit...
pause>nul
exit /b

:BUILDPROPNOTFOUND
echo BUILD.PROP not found! Please unpack SYSTEM partition file before run.
echo Press any key to exit...
pause>nul
exit /b

:CFGNOTFOUND
echo CFG file not found! Please unpack SYSTEM partition file before run.
echo Press any key to exit...
pause>nul
exit /b
