::SCRIPTBIN_CONVERT script by BNDias and petesimon.
::Convert hardware configuration file of the camera into text file and vice versa.
::Check GoPrawn.com for details.

@echo off
echo SCRIPTBIN_CONVERT.BAT Convertion of hardware configuration file by BNDias and petesimon for GoPrawn.com
echo.

:PROMPT
echo Perform conversion:
echo    1) script.bin to script.fex
echo    2) script.fex to script.bin
echo    0) cancel
:PROMPTOPTION
set /p input=Option: 
if /i "%input%" EQU "1" goto BIN2FEX
if /i "%input%" EQU "2" goto FEX2BIN
if /i "%input%" EQU "0" goto EXITBAT
goto PROMPTOPTION



:BIN2FEX
if not exist "%~dp0script.bin"  goto SCRIPTBINNOTFOUND
if not exist "%~dp0Tools\script_bin\bin2fex.jar"  goto BIN2FEXNOTFOUND
where java.exe > NUL
if %ERRORLEVEL% GTR 0  goto JAVANOTFOUND

echo.
echo.
if exist "%~dp0script.fex" (
	if exist "%~dp0script.fex.bak"  del /F "%~dp0script.fex.bak"
	ren "%~dp0script.fex" "script.fex.bak">nul
	echo Existing SCRIPT.FEX file renamed to SCRIPT.FEX.BAK
)

echo.
echo Converting binary "script.bin" hardware description data to readable text...

java -jar "%~dp0Tools\script_bin\bin2fex.jar" "%~dp0script.bin" 2>NUL| more > "%~dp0script.fex"

start notepad "%~dp0script.fex"
echo Done. Check SCRIPT.FEX file.
goto EXITBAT


:FEX2BIN
if not exist "%~dp0script.fex"  goto SCRIPTFEXNOTFOUND
if not exist "%~dp0Tools\script_bin\fex2bin.exe"  goto FEX2BINNOTFOUND

echo.
echo.
if exist "%~dp0script.bin" (
	if exist "%~dp0script.bin.bak"  del /F "%~dp0script.bin.bak"
	ren "%~dp0script.bin" "script.bin.bak">nul
	echo Existing SCRIPT.BIN file renamed to SCRIPT.BIN.BAK
)

echo.
echo Converting "script.fex" text file to binary data...

"%~dp0Tools\script_bin\fex2bin.exe" "%~dp0script.fex" 2>NUL

echo Done. Check SCRIPT.BIN file.
goto EXITBAT











:EXITBAT
echo Press any key to exit...
pause>nul
exit /b


:SCRIPTBINNOTFOUND
echo SCRIPT.BIN hardware configuration file not found.
goto EXITBAT

:BIN2FEXNOTFOUND
echo BIN2FEX.JAR tool not found.
goto EXITBAT

:JAVANOTFOUND
echo Java executable not installed or not found - Please download it from: https://www.java.com
goto EXITBAT

:SCRIPTFEXNOTFOUND
echo SCRIPT.FEX hardware configuration file not found.
goto EXITBAT

:SCRIPTNOTFOUND
echo SCRIPT.EXE tool not found.
goto EXITBAT
