::REPACK_BOOTIMG.BAT script by BNDias.
::Check GoPrawn.com for details.

@echo off

::Set source folder and destination file
set SOURCE=BOOT-ROOT
set DESTINATION=UNFEX\1-boot.img
set DESTSIZE=2883584
::Set BOOTIMG tool folder
set TOOLFOLDER=Tools\unpack_repack_bootimg_kernel


echo UNPACK_BOOTIMG.BAT script by BNDias for GoPrawn.com
echo.


::Change working directory
set ROOT=%~dp0
cd "%ROOT%"

if not exist "%TOOLFOLDER%\bootimg.exe"  goto BOOTIMGNOTFOUND
if not exist "Tools\sfk.exe"  goto SFKNOTFOUND
if not exist "%SOURCE%"  goto SOURCENOTFOUND


setlocal EnableDelayedExpansion

if exist "%DESTINATION%" (
    if exist "%DESTINATION%.bak"  del /F "%DESTINATION%.bak"
    for %%A in ("%DESTINATION%") do  set DESTFILENAME=%%~nxA
    ren "%DESTINATION%" "!DESTFILENAME!.bak">nul
    echo Existing %DESTINATION% file renamed to !DESTFILENAME!.bak
    echo.
)


::Set exclude list (not extracted files/folders)
if exist "%TOOLFOLDER%\ExcludeList.txt" del "%TOOLFOLDER%\ExcludeList.txt" >nul 2>&1
for %%f in ("%TOOLFOLDER%\*") do  echo "%%f" >> "%TOOLFOLDER%\ExcludeList.txt"
for /D %%d in ("%TOOLFOLDER%\*") do  echo "%%d" >> "%TOOLFOLDER%\ExcludeList.txt"
echo "%TOOLFOLDER%\ExcludeList.txt" >> "%TOOLFOLDER%\ExcludeList.txt"


::Copy source content to tool folder
if exist "%TOOLFOLDER%\boot-root"  rd /S /Q "%TOOLFOLDER%\boot-root" >nul 2>&1
xcopy "%SOURCE%" "%TOOLFOLDER%" /e /v /r /y /i /k >nul


::Create BOOT.IMG
cd "%TOOLFOLDER%"
echo Repacking %source%...
echo ==================================================
bootimg --repack-bootimg
echo ==================================================
cd "%ROOT%"


::Move destination file to tool folder
for %%A in ("%DESTINATION%") do  set DESTFOLDERNAME=%%~fA
move /Y "%TOOLFOLDER%\boot-new.img" "%DESTFOLDERNAME%" >nul 2>&1


::Remove source content from tool folder
for %%f in ("%TOOLFOLDER%\*") do (
    find "%%f" "%TOOLFOLDER%\ExcludeList.txt" >nul
    if !ERRORLEVEL! == 1  del /F /Q "%%f" >nul 2>&1
)
for /D %%d in ("%TOOLFOLDER%\*") do (
    find "%%d" "%TOOLFOLDER%\ExcludeList.txt" >nul
    if !ERRORLEVEL! == 1  del /F /Q "%%d" >nul 2>&1
)
::Remove exclude list
del /F /Q "%TOOLFOLDER%\ExcludeList.txt" >nul 2>&1


for %%A in ("%DESTINATION%") do set size=%%~zA
if %size% GTR %DESTSIZE% goto BADFILESIZE
if %size% LSS %DESTSIZE% (
    if not exist "Tools\DummyFiles\rootdummy.bin"  goto DUMMYNOTFOUND
    copy /b "%DESTINATION%"+"Tools\DummyFiles\rootdummy.bin" "boot.tmp" >nul 2>&1
    Tools\sfk "partcopy" "boot.tmp" "0" "%DESTSIZE%" "%DESTINATION%" "-yes" "-quiet"
    del /F /Q "boot.tmp" >nul 2>&1
)

echo.
echo Done. Check "%DESTINATION%" file.
endlocal


:EXITBAT
echo Press any key to exit...
pause>nul
exit /b


:BOOTIMGNOTFOUND
echo.
echo BOOTIMG.EXE not found
goto EXITBAT

:SFKNOTFOUND
echo SFK.EXE not found - Please download it from: https://sourceforge.net/projects/swissfileknife/
goto EXITBAT

:SOURCENOTFOUND
echo %SOURCE% folder not found
goto EXITBAT

:BADFILESIZE
echo Error! The size of "%DESTINATION%" file exceeds %DESTSIZE% bytes.
del /F /Q "%DESTINATION%">nul
goto EXITBAT

:DUMMYNOTFOUND
echo ROOTDUMMY.BIN file not found
del /F /Q "%DESTINATION%">nul
goto EXITBAT