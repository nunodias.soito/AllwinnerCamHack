::BACKUP Get firmware backup file and partition files of the camera.
::Works with ADB tool.
::Check GoPrawn.com for details.

@echo off
echo Allwinner V3 action cam firmware backup script by nutsey for GoPrawn.com
echo.

::Check device connection state
adb get-state 1>nul 2>&1
::adb devices -l | find "device product:" >nul
if %ERRORLEVEL% == 1  goto DEVICENOTFOUND

echo Be sure you have a SD card inserted into the camera.
echo Press any key to continue...
pause>nul
echo.
echo Backing up firmware files...
echo ==========================================================
adb kill-server && adb start-server
adb remount
adb shell cd /
adb shell rm -r /mnt/extsd/backup
adb shell mkdir /mnt/extsd/backup
echo Backup folder created.
echo.
adb shell dd if=/dev/block/mtdblock0 of=/mnt/extsd/backup/0-uboot.img
echo Block 0 copied.
echo.
adb shell dd if=/dev/block/mtdblock1 of=/mnt/extsd/backup/1-boot.img
echo Block 1 copied.
echo.
adb shell dd if=/dev/block/mtdblock2 of=/mnt/extsd/backup/2-system.img
echo Block 2 copied.
echo.
adb shell dd if=/dev/block/mtdblock3 of=/mnt/extsd/backup/3-config.img
echo Block 3 copied.
echo.
adb shell dd if=/dev/block/mtdblock4 of=/mnt/extsd/backup/4-blogo.img
echo Block 4 copied.
echo.
adb shell dd if=/dev/block/mtdblock5 of=/mnt/extsd/backup/5-slogo.img
echo Block 5 copied.
echo.
adb shell dd if=/dev/block/mtdblock6 of=/mnt/extsd/backup/6-env.img
echo Block 6 copied.
echo.
adb pull /mnt/extsd/backup backup
echo All blocks downloaded.
echo ==========================================================
cd backup
echo Building full_img.fex...
copy /b 0-uboot.img+1-boot.img+2-system.img+3-config.img+4-blogo.img+5-slogo.img+6-env.img full_img.fex
echo.
echo Done. Check BACKUP folder of SD Card.

cd ..
:: ZIP BACKUP FILES (reference http://ss64.com/nt/)
if not exist zip.exe  goto :EXITBAT
echo.
set /p zipanswer=Compress all backup files together as 'backup.zip'? [Y/N]
if /i "%zipanswer:~,1%" NEQ "Y"  goto EXITBAT
if exist backup.zip (
	echo Existing BACKUP.ZIP file renamed to BACKUP.ZIP.BAK
	ren backup.zip backup.zip.bak
)
echo Compressing backup files...
type zipComment.txt | zip -z9 backup.zip backup\0-uboot.img backup\1-boot.img backup\2-system.img backup\3-config.img backup\4-blogo.img backup\5-slogo.img backup\6-env.img backup\full_img.fex
echo Done. Files compressed into BACKUP.ZIP.

:EXITBAT
echo Press any key to exit...
pause>nul
exit /b

:DEVICENOTFOUND
echo DEVICE NOT FOUND! Please connect the camera to your PC.
goto EXITBAT