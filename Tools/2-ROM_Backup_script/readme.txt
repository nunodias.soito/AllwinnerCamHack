Allwinner V3 action cam firmware backup instructions:
  1. Connect your camera to PC with a USB cable. Select 'Charging mode' and press OK.
  2. Disable driver signature verification and install ADB driver for the new device.
  3. Run 'backup.cmd' script. Wait until done.

Your firmware backup file and partition files will be located in 'backup' folder on SD Card.


GETSCRIPTBIN instructions:  =======================================================================
  1. Connect your camera to PC with a USB cable. Select 'Charging mode' and press OK.
  2. Run 'getscriptbin.bat' script. Wait until done.

Your hardware configuration file will be located in 'backup' folder of SD Card.
===================================================================================================


Be sure you have a SD card inserted into the camera before run this scripts.
Check goprawn.com action cam discussion forum for more details.